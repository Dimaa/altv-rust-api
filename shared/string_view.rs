/**
 * 
 */

use altv_capi::{
    alt_StringView
};

use std::{
    ffi::{
        CString,
        CStr
    }
};

pub fn from(astr : *mut alt_StringView) -> String
{
    return unsafe {
        CStr::from_ptr((*astr).data)
            .to_str().unwrap().to_owned()
    };
}

pub fn new(string: &str) -> alt_StringView
{
    let cstr = CString::new(string).expect("CString::new failed");

    let astr = alt_StringView {
        data: cstr.into_raw(),
        size: string.len() as u64
    };

    return astr;
}
