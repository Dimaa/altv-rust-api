
use API;
use string_view;
use altv_capi;

/// Logs info message
// pub fn info(api: API, msg: &str)
// {
//     let mut sv = string_view::new(msg);
//     unsafe { altv_capi::alt_IServer_LogInfo(api, &mut sv); }
// }

/// Logs info message
pub fn info(msg: &str)
{
    unsafe {
        let api = altv_capi::alt_IServer_Instance();
        let mut sv = string_view::new(msg);
        altv_capi::alt_IServer_LogInfo(api, &mut sv);
    }
}
#[macro_export]
macro_rules! logi {
    () => ($crate::log::info(""));

    ($($arg:tt)*) => ({
        $crate::log::info(&format!($($arg)*).to_owned());
    })
}

pub fn error(msg: &str)
{
    unsafe {
        let api = altv_capi::alt_IServer_Instance();
        let mut sv = string_view::new(msg);
        altv_capi::alt_IServer_LogError(api, &mut sv);
    }
}
#[macro_export]
macro_rules! loge {
    () => ($crate::log::info(""));

    ($($arg:tt)*) => ({
        $crate::log::error(&format!($($arg)*).to_owned());
    })
}

pub fn warning(msg: &str)
{
    unsafe {
        let api = altv_capi::alt_IServer_Instance();
        let mut sv = string_view::new(msg);
        altv_capi::alt_IServer_LogWarning(api, &mut sv);
    }
}
#[macro_export]
macro_rules! logw {
    () => ($crate::log::info(""));

    ($($arg:tt)*) => ({
        $crate::log::warning(&format!($($arg)*).to_owned());
    })
}

pub fn debug(msg: &str)
{
    unsafe {
        let api = altv_capi::alt_IServer_Instance();
        let mut sv = string_view::new(msg);
        altv_capi::alt_IServer_LogDebug(api, &mut sv);
    }
}
#[macro_export]
macro_rules! logd {
    () => ($crate::log::info(""));

    ($($arg:tt)*) => ({
        $crate::log::debug(&format!($($arg)*).to_owned());
    })
}

pub fn colored(msg: &str)
{
    unsafe {
        let api = altv_capi::alt_IServer_Instance();
        let mut sv = string_view::new(msg);
        altv_capi::alt_IServer_LogColored(api, &mut sv);
    }
}
#[macro_export]
macro_rules! logc {
    () => ($crate::log::info(""));

    ($($arg:tt)*) => ({
        $crate::log::colored(&format!($($arg)*).to_owned());
    })
}
