/**
 *
 */

use callbacks;
use altv_capi::*;

pub fn new(
    info : * mut alt_IResource_CreationInfo,
    on_make_client : callbacks::OnMakeClientFn,
    on_instantiate : callbacks::OnInstantiateFn,
    on_start : callbacks::OnStartFn,
    on_stop : callbacks::OnStopFn,
    on_event : callbacks::OnEventFn,
    on_tick : callbacks::OnTick,
    on_create_base_object : callbacks::OnCreateBaseObjectFn,
    on_remove_base_object : callbacks::OnRemoveBaseObjectFn
) -> * mut alt_IResource
{
    unsafe {
        return alt_CAPIResource_Create(
            info,
            Some(on_make_client),
            Some(on_instantiate),
            Some(on_start),
            Some(on_stop),
            Some(on_event),
            Some(on_tick),
            Some(on_create_base_object),
            Some(on_remove_base_object)
        );
    }
}
