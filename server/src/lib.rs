/**
 * 
 */

#[allow(non_upper_case_globals)]
#[allow(non_camel_case_types)]
#[allow(non_snake_case)]
#[allow(dead_code)]
pub mod altv_capi;

#[path = "../../shared/string_view.rs"]
pub mod string_view;
#[path = "../../shared/string.rs"]
pub mod string;

pub type API = altv_capi::alt_IServer;

pub mod log;

#[path = "../../shared/callbacks.rs"]
mod callbacks;

pub mod resource;
#[path = "../../shared/runtime.rs"]
pub mod runtime;

/// This function should be called in the beginning of altMain!
pub fn set_api(api: *mut API)
{
    unsafe { altv_capi::alt_IServer_SetInstance(api); }
}

pub fn register_runtime(
    api: *mut API,
    resourceType: &str,
    runtime: *mut altv_capi::alt_IScriptRuntime
) -> bool
{
    let mut typesv = string_view::new(resourceType);

    unsafe {
        return altv_capi::alt_IServer_RegisterScriptRuntime(
            api, 
            &mut typesv,
            runtime
        );
    }
}

pub fn get_version() -> u32
{
    unsafe { return altv_capi::alt_GetSDKVersion() as u32; }
}
